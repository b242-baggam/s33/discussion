// JavaScript Synchronous vs Asynchronous
// Synchronous - meaning only one statement is executed at a time
// Asynchronous - means that we can proceed to execute other statements while time consuming code is running in the background.

console.log("Hello");
// conosle.log("Hello again");

// Getting all posts
/*
	Syntax:
		fetch('URL');
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Checking the status of the request
/*
	fetch('URL')
	.then((response) => {})
*/
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

// Retrieve the contents/data from the "Response" object

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json));

// Creates a function using "async" and "await" keywords.

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result returns a promise
	console.log(result);
	// Returns the type of the response
	console.log(typeof result);
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	console.log(json);
}

fetchData();